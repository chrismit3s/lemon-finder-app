import "dart:math" as math;

import "package:flutter_barcode_scanner/flutter_barcode_scanner.dart";


class Pair<A, B> {
  final A a;
  final B b;

  const Pair(this.a, this.b);

  A get first => this.a;
  B get second => this.b;

  dynamic operator [] (int i) {
    if (i == 0)
      return this.first;
    else if (i == 1)
      return this.second;
    else
      throw RangeError("Out of bounds index for pair: $i");
  }
}

class Triple<A, B, C> {
  final A a;
  final B b;
  final C c;

  const Triple(this.a, this.b, this.c);

  A get first => this.a;
  B get second => this.b;
  C get third => this.c;

  dynamic operator [] (int i) {
    if (i == 0)
      return this.first;
    else if (i == 1)
      return this.second;
    else if (i == 2)
      return this.third;
    else
      throw RangeError("Out of bounds index for triple: $i");
  }
}


int argmax(List<num> l) {
  if (l.length == 0)
    throw ArgumentError.value(l, "l", "must not be empty");

  int index = 0;
  for (int i = 1; i != l.length; ++i)
    if (l[index] < l[i])
      index = i;
  return index;
}

num listmax(List<num> l) {
  if (l == null || l.length == 0)
    throw ArgumentError("List must contain at least one element");

  return l.fold(l[0], math.max);
}

List<Pair<A, B>> zip2<A, B>(List<A> as, List<B> bs) {
  int len = math.min(as.length, bs.length);

  var res = List<Pair<A, B>>();
  for (int i = 0; i != len; ++i)
    res.add(Pair<A, B>(as[i], bs[i]));

  return res;
}

List<Triple<A, B, C>> zip3<A, B, C>(List<A> as, List<B> bs, List<C> cs) {
  int len = math.min(math.min(as.length, bs.length), cs.length);

  var res = List<Triple<A, B, C>>();
  for (int i = 0; i != len; ++i)
    res.add(Triple<A, B, C>(as[i], bs[i], cs[i]));

  return res;
}

List<int> range(int end) {
  return List<int>.generate(end, (i) => i);
}

List<Pair<int, T>> enumerate<T>(List<T> l) {
  return zip2(range(l.length), l);
}


Future<String> scanQrCode() async {
  return FlutterBarcodeScanner.scanBarcode(
    "#FF0000",  // a red scan line
    "CANCEL",  // cancel button text
    false,  // no flash icon
    ScanMode.QR,
  ) ?? "";
}
