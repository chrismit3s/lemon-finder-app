#ifndef CONVERT_H
#define CONVERT_H

#include <stdint.h>

void convert_from_yuv420(uint32_t*, uint8_t*, uint8_t*, uint8_t*, int, int, int, int);

#endif /* CONVERT_H */
