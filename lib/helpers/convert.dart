import "dart:ffi";
import "dart:io" show Platform;

import "package:image/image.dart" as imglib;
import "package:camera/camera.dart" as camlib;
import "package:ffi/ffi.dart";


typedef ConvertNative = Void Function(Pointer<Uint32>, Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, Int32, Int32, Int32, Int32);
typedef ConvertFunc = void Function(Pointer<Uint32>, Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, int, int, int, int);


final convertLib = Platform.isAndroid ? DynamicLibrary.open("libconvert.so") : DynamicLibrary.process();
final ConvertFunc _convertFromYUV420Native = convertLib
        .lookup<NativeFunction<ConvertNative>>("convert_from_yuv420")
        .asFunction<ConvertFunc>();


class Converter {
  static final resolutionLookup = {
    camlib.ResolutionPreset.low: Platform.isAndroid ? <int>[320, 240] : <int>[352, 288],
    camlib.ResolutionPreset.medium: Platform.isAndroid ? <int>[720, 480] : <int>[640, 480],
    camlib.ResolutionPreset.high: <int>[1280, 720],
    camlib.ResolutionPreset.veryHigh: <int>[1920, 1080],
    camlib.ResolutionPreset.ultraHigh: <int>[3840, 2160],
    camlib.ResolutionPreset.max: <int>[3840, 2160],
  };

  final bool isYuv420;
  final List<int> res;
  List<Pointer<Uint8>> planeBufs;
  Pointer<Uint32> imageBuf;

  Converter(camlib.ImageFormatGroup format, camlib.ResolutionPreset res) :
      res = Converter.resolutionLookup[res],
      isYuv420 = (format == camlib.ImageFormatGroup.yuv420) {

    if (format != camlib.ImageFormatGroup.bgra8888 && format != camlib.ImageFormatGroup.yuv420)
        throw FormatException("Image is of unknown format " + format.toString());

    if (this.isYuv420) {
      final size = this.res[0] * this.res[1];
      this.imageBuf = allocate<Uint32>(count: size);
      this.planeBufs = <Pointer<Uint8>>[
        allocate<Uint8>(count: size),
        allocate<Uint8>(count: size), // ~/ 4),  // size / 4 is always a whole number
        allocate<Uint8>(count: size)]; // ~/ 4)];
    }
  }

  imglib.Image convertFromYuv420(camlib.CameraImage image) {
    // create pointer to each plane
    for (int i = 0; i != this.planeBufs.length; ++i) {
      final planeBytes = image.planes[i].bytes;
      final planeLen = planeBytes.length;
      this.planeBufs[i].asTypedList(planeLen).setRange(0, planeLen, planeBytes);
    }

    // check image size
    if (image.width != this.res[0] || image.height != this.res[1])
      print("PANIC - SIZE MISMATCH - EXPECTED (" + this.res[0].toString() + "|" + this.res[1].toString() + "), GOT (" + image.width.toString() + "|" + image.height.toString() + ")");

    // call c function
    final data = _convertFromYUV420Native(
        this.imageBuf,
        this.planeBufs[0], this.planeBufs[1], this.planeBufs[2],
        image.planes[1].bytesPerPixel ?? 1, image.planes[1].bytesPerRow,
        this.res[0], this.res[1]);

    // create image from converted pixel values
    return imglib.Image.fromBytes(
      this.res[0], this.res[1],
      this.imageBuf.asTypedList(this.res[0] * this.res[1]),
    );
  }

  imglib.Image convertFromBgra8888(camlib.CameraImage image) {
    return imglib.Image.fromBytes(
      image.width,
      image.height,
      image.planes[0].bytes,
      format: imglib.Format.bgra,
    );
  }

  imglib.Image camlibToImglib(camlib.CameraImage image) {
    if (this.isYuv420)
      return this.convertFromYuv420(image);
    else
      return this.convertFromBgra8888(image);
  }

  void dispose() {
    if (this.isYuv420) {
      free(this.imageBuf);
      for (int i = 0; i != this.planeBufs.length; ++i)
        free(this.planeBufs[i]);
    }
  }
}
