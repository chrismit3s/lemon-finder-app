#include "convert.h"

#include <stdlib.h>


static inline unsigned char clamp_byte(int n) {
	return (-(n >= 0) & n) | -(n >= 255);
}

void convert_from_yuv420(
		uint32_t *rgba,
		uint8_t *ys, uint8_t *us, uint8_t *vs,
		int bytes_per_pixel, int bytes_per_row,
		int width, int height) {
	unsigned char r, g, b;
	unsigned int a_shifted = 0xFF000000;
	int y, u, v;
	int index, uv_index;
	for (int i = 0; i != width; ++i) {
		for (int j = 0; j != height; ++j) {
			index = i + j * width;
			uv_index = bytes_per_pixel * (i / 2) + bytes_per_row * (j / 2);

			y = ys[index];
			u = us[uv_index];
			v = vs[uv_index];

			r = clamp_byte(y + v * 1436.0 / 1024 - 179);
			g = clamp_byte(y - u * 46549.0 / 131072 - v * 93604.0 / 131072 + 135);
			b = clamp_byte(y + u * 1814.0 / 1024 - 227);

			rgba[index] = a_shifted | (b << 16) | (g << 8) | r;
		}
	}
}
