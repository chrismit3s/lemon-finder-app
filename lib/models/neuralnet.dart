import "dart:async";
import "dart:io";

import "package:flutter/material.dart";
import "package:flutter/foundation.dart";
import "package:image/image.dart" as imglib;
import "package:tflite_flutter/tflite_flutter.dart" as tfl;
import "package:tflite_flutter_helper/tflite_flutter_helper.dart" as tflh;


enum DataFormat { channelsLast, channelsFirst }

enum DelegateType { none, nnapi, gpuv2, metal }


class NeuralNet extends ChangeNotifier {
  final dataFormat = DataFormat.channelsLast;

  final labels = <String>[  // for emojis see https://unicode.org/emoji/charts/full-emoji-list.html
    "Apple\u{1F34E}",  // red apple emoji
    "Banana\u{1F34C}",  // banana emoji
    "Lemon\u{1F34B}",  // lemon emoji
    "No match\u{1F937}\u{200D}\u{2642}\u{FE0F}",  // man shrugging emoji
  ];
  final classes = <String>["apple", "banana", "lemon"];
  final no_match_class = "none-of-the-above";
  List<String> get all_classes => this.classes + [this.no_match_class];

  tflh.ImageProcessor imagePrep;
  tfl.Delegate delegate;
  tfl.Interpreter interpreter;

  DelegateType delegateType;
  String model;

  Completer<void> completer = Completer<void>();
  bool get initialized => this.completer.isCompleted;
  Future<void> get initializing => this.completer.future;

  List<int> inputShape;
  List<int> get imageSize {
    if (this.dataFormat == DataFormat.channelsLast)
      return <int>[this.inputShape[0], this.inputShape[1]];
    else
      return <int>[this.inputShape[1], this.inputShape[2]];
  }


  NeuralNet();

  NeuralNet.createAndUpdate(String model, DelegateType delegateType) {
    this.update(model, delegateType);
  }


  Future<void> initialize() async {
    if (this.initialized)
      return;

    // create gpu delegate
    final options = tfl.InterpreterOptions();
    switch (this.delegateType) {
      case DelegateType.none:
        print("[NeuralNet.initialize] Using no delegate");
        this.delegate = null;
        break;

      case DelegateType.nnapi:
        print("[NeuralNet.initialize] Using NNAPI delegate");
        this.delegate = tfl.NnApiDelegate();
        options.addDelegate(this.delegate);
        break;

      case DelegateType.gpuv2:
        print("[NeuralNet.initialize] Using GPUv2 delegate");
        this.delegate = tfl.GpuDelegateV2(
          options: tfl.GpuDelegateOptionsV2(
            false,
            tfl.TfLiteGpuInferenceUsage.fastSingleAnswer,
            tfl.TfLiteGpuInferencePriority.minLatency,  // only priority: fast answers
            tfl.TfLiteGpuInferencePriority.auto,
            tfl.TfLiteGpuInferencePriority.auto,
          ),
        );
        options.addDelegate(this.delegate);
        break;

      case DelegateType.metal:
        print("[NeuralNet.initialize] Using Metal delegate");
        this.delegate = tfl.GpuDelegate(
          options: tfl.GpuDelegateOptions(
            false,  // allow precision loss
            tfl.TFLGpuDelegateWaitType.active,  // minimize latency
          ),
        );
        options.addDelegate(this.delegate);
        break;
    }

    // create interpreter
    print("[NeuralNet.initialize] Creating interpreter from ${this.model}");
    this.interpreter = await tfl.Interpreter.fromAsset(this.model, options: options);

    // get interpreter input shape
    this.inputShape = this.interpreter.getInputTensor(0).shape.sublist(1);  // starting from 1 because the dim 0 is the batch dim

    // create image preprocessor
    this.imagePrep = tflh.ImageProcessorBuilder()
      .add(tflh.ResizeOp(this.imageSize[0], this.imageSize[1], tflh.ResizeMethod.BILINEAR))
      .add(tflh.NormalizeOp(0.0, 255.0))
      .build();

    // initialization complete
    this.completer.complete(null);
  }

  void checkInit() {
    if (!this.initialized)
      throw StateError("NeuralNet not initialized");
  }

  void update(String model, DelegateType delegateType) {
    // nothing changed
    if (this.model == model && this.delegateType == delegateType)
      return;

    // dispose current controller and initialize new one after that
    print("[NeuralNet.update] Disposing this");
    this.dispose();

    // update config
    this.model = model;
    this.delegateType = delegateType;

    // re-initialize
    print("[NeuralNet.update] Initializing this");
    this.initialize();

    this.notifyListeners();
  }

  List<double> predict(imglib.Image image) {
    this.checkInit();

    var input = this.imagePrep.process(tflh.TensorImage()..loadImage(image));
    var output = tflh.TensorBuffer.createFixedSize(<int>[this.labels.length], tfl.TfLiteType.float32);
    this.interpreter.run(input.buffer, output.buffer);
    return output.getDoubleList();
  }

  void dispose() {
    // replace completer by an incomplete one, as from now on the object is disposed
    this.completer = Completer<void>();

    this.interpreter?.close();
    this.interpreter = null;

    this.delegate?.delete();
    this.delegate = null;
  }
}
