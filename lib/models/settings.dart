import "dart:io";
import "dart:convert";

import "package:camera/camera.dart" as camlib;
import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:shared_preferences/shared_preferences.dart";

import "package:lemonfinderapp/models/neuralnet.dart";


class Settings extends ChangeNotifier {
  static final revision = 4;

  static final cameraNameLookup = {
    camlib.CameraLensDirection.back: "back-facing camera",
    camlib.CameraLensDirection.front: "front-facing camera",
    camlib.CameraLensDirection.external: "external camera",
  };

  static final resolutionNameLookup = {
    camlib.ResolutionPreset.low: "low (320p)",
    camlib.ResolutionPreset.medium: "medium (480p)",
    camlib.ResolutionPreset.high: "HD (720p)",
    camlib.ResolutionPreset.veryHigh: "FullHD (1080p)",
    camlib.ResolutionPreset.ultraHigh: "4K (2160p)",
    camlib.ResolutionPreset.max: "best",
  };

  static final delegateNameLookup = {
    DelegateType.none: "none",
    DelegateType.nnapi: "NNAPI",
    DelegateType.gpuv2: "GPU v2",
    DelegateType.metal: "Metal",
  };
  static final delegateDescriptionLookup = {
    DelegateType.none: "no delegate",
    DelegateType.nnapi: "use the NN API (Android only)",
    DelegateType.gpuv2: "use GPU acceleration (Android only)",
    DelegateType.metal: "use the Metal API (iOS only)",
  };
  static final delegateAvailableLookup = {
    DelegateType.none: true,
    DelegateType.nnapi: Platform.isAndroid,
    DelegateType.gpuv2: Platform.isAndroid,
    DelegateType.metal: Platform.isIOS,
  };

  static final onTapInterval = -1;
  static final onTapClearDelay = 5;
  static final intervalNameLookup = {
    Settings.onTapInterval: "only on tap",
    1: "every 1 second",
    2: "every 2 seconds",
    3: "every 3 seconds",
    4: "every 4 seconds",
    5: "every 5 seconds",
  };
  static final intervalDescriptionLookup = {
    Settings.onTapInterval: "tap the image to trigger a scan",
    1: "very fast (may cause severe performance loss and low frame rates)",
    2: "fast (may cause severe performance loss and low frame rates)",
    3: "normal (recommended)",
    4: "slow",
    5: "very slow",
  };


  Future<List<camlib.CameraDescription>> availableCamerasFuture;
  List<camlib.CameraDescription> availableCameras;

  List<camlib.ResolutionPreset> get availableResolutions => camlib.ResolutionPreset.values;
  List<DelegateType> get availableDelegates => DelegateType.values;
  List<int> get availableIntervals => <int>[Settings.onTapInterval, 1, 2, 3, 4, 5];

  Future<String> assetManifestFuture;
  String assetManifest;

  List<String> get availableModels {
    return json
        .decode(this.assetManifest)
        .keys
        .where((String key) => key.endsWith(".tflite"))
        .map<String>((String key) => key.split("/").last)
        .toList();
  }


  Future<SharedPreferences> prefsFuture;
  SharedPreferences prefs;

  bool initialized = false;
  Future<void> initializing;


  Settings() {
    this.prefsFuture = SharedPreferences.getInstance();
    this.availableCamerasFuture = camlib.availableCameras();
    this.assetManifestFuture = rootBundle.loadString("AssetManifest.json");

    this.initialized = false;
    this.initializing = this.initialize();
  }

  Future<void> initialize() async {
    if (this.initialized)
      return;

    this.prefs = await this.prefsFuture;
    this.availableCameras = await this.availableCamerasFuture;
    this.assetManifest = await this.assetManifestFuture;

    // clear all settings if the settings revision changed (some key name changed, etc) and set the defaults
    if (this.revisionChanged()) {
      this.prefs.clear();
      this.prefs.setInt("revision", Settings.revision);
    }
    this.setDefaults();

    this.initialized = true;
    this.notifyListeners();
  }

  void checkInit() {
    if (!this.initialized)
      throw StateError("Settings not initialized");
  }


  bool revisionChanged() {
    return this.prefs.getInt("revision") != Settings.revision;
  }

  void setDefaults() {
    this.setDefaultBool("showModelInput", false);
    this.setDefaultInt("camera", 0);
    this.setDefaultInt("resolution", 0);
    this.setDefaultString("model", this.availableModels[0]);
    this.setDefaultInt("activeDelegate", 0);
    this.setDefaultInt("scanInterval", Settings.onTapInterval);
    this.setDefaultString("username", "user");
    this.setDefaultString("server", "0.0.0.0");
    this.setDefaultInt("port", 42069);
  }

  void setDefaultInt(String key, int value) {
    if (!this.prefs.containsKey(key)) {
      this.prefs.setInt(key, value);
      this.notifyListeners();
    }
  }

  void setDefaultBool(String key, bool value) {
    if (!this.prefs.containsKey(key)) {
      this.prefs.setBool(key, value);
      this.notifyListeners();
    }
  }

  void setDefaultString(String key, String value) {
    if (!this.prefs.containsKey(key)) {
      this.prefs.setString(key, value);
      this.notifyListeners();
    }
  }


  void updateInt(String key, int value) {
    if (value == null)
      return;
    if (value != this.prefs.getInt(key)) {
      this.prefs.setInt(key, value);
      this.notifyListeners();
    }
  }

  void updateBool(String key, bool value) {
    if (value == null)
      return;
    if (value != this.prefs.getBool(key)) {
      this.prefs.setBool(key, value);
      this.notifyListeners();
    }
  }

  void updateString(String key, String value) {
    if (value == null || value.length == 0)
      return;
    if (value != this.prefs.getString(key)) {
      this.prefs.setString(key, value);
      this.notifyListeners();
    }
  }


  bool get showModelInput {
    this.checkInit();
    return this.prefs.getBool("showModelInput") ?? false;
  }

  void set showModelInput(bool value) {
    this.checkInit();
    this.updateBool("showModelInput", value);
  }


  camlib.CameraDescription get camera {
    this.checkInit();

    final int index = this.prefs.getInt("camera") ?? 0;
    if (index < 0 || index >= this.availableCameras.length)
      throw ArgumentError("Cannot get camera description (index=$index)");

    return this.availableCameras[index];
  }

  void set camera(camlib.CameraDescription value) {
    this.checkInit();

    final int index = this.availableCameras.indexOf(value);
    if (index < 0 || index >= this.availableCameras.length)
      throw ArgumentError("Cannot set unknown camera description (index=$index, value=$value)");

    this.updateInt("camera", index);
  }


  camlib.ResolutionPreset get resolution {
    this.checkInit();

    final int index = this.prefs.getInt("resolution") ?? 0;
    if (index < 0 || index >= this.availableResolutions.length)
      throw ArgumentError("Cannot get camera resolution (index=$index)");

    return this.availableResolutions[index];
  }

  void set resolution(camlib.ResolutionPreset value) {
    this.checkInit();

    final int index = this.availableResolutions.indexOf(value);
    if (index < 0 || index >= this.availableResolutions.length)
      throw ArgumentError("Cannot set unknown camera resolution (index=$index, value=$value)");

    this.updateInt("resolution", index);
  }


  String get model {
    this.checkInit();
    return this.prefs.getString("model") ?? this.availableModels[0];
  }

  void set model(String value) {
    this.checkInit();
    this.updateString("model", value);
  }


  DelegateType get activeDelegate {
    this.checkInit();

    final int index = this.prefs.getInt("activeDelegate") ?? 0;
    if (index < 0 || index >= this.availableDelegates.length)
      throw ArgumentError("Cannot get delegate (index=$index)");

    return this.availableDelegates[index];
  }

  void set activeDelegate(DelegateType value) {
    this.checkInit();

    final int index = this.availableDelegates.indexOf(value);
    if (index < 0 || index >= this.availableDelegates.length)
      throw ArgumentError("Cannot set unknown delegate (index=$index, value=$value)");

    this.updateInt("activeDelegate", index);
  }


  int get scanInterval {
    this.checkInit();
    return this.prefs.getInt("scanInterval");
  }

  void set scanInterval(int value) {
    this.checkInit();
    this.updateInt("scanInterval", value);
  }

  bool get scanOnTap => (this.scanInterval == Settings.onTapInterval);


  String get username {
    this.checkInit();
    return this.prefs.getString("username");
  }

  void set username(String value) {
    this.checkInit();
    this.updateString("username", value);
  }


  String get server {
    this.checkInit();
    return this.prefs.getString("server");
  }

  void set server(String value) {
    this.checkInit();
    this.updateString("server", value);
  }


  int get port {
    this.checkInit();
    return this.prefs.getInt("port");
  }

  void set port(int value) {
    this.checkInit();
    this.updateInt("port", value);
  }
}
