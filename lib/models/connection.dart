import "dart:io";
import "dart:convert";
import "dart:async";

import "package:flutter/material.dart";

import "package:http/http.dart";


enum ConnectionStatus { disconnected, connecting, connected, serverUnreachable, badUsername }


class Connection extends ChangeNotifier {
  static final connectionTimeout = 5;  // seconds

  String username;
  String url;
  int port;

  Client client;
  String token;
  ConnectionStatus status = ConnectionStatus.disconnected;

  String get statusMessage {
    switch (this.status) {
      case ConnectionStatus.disconnected:
        return "disconnected";
      case ConnectionStatus.connecting:
        return "connecting";
      case ConnectionStatus.connected:
        return "connected to ${this.url}";
      case ConnectionStatus.serverUnreachable:
        return "cannot connect to ${this.url} on port ${this.port}";
      case ConnectionStatus.badUsername:
        return "username '${this.username}' was rejected";
    }
  }

  String get urlbase => "http://${this.url}:${this.port}";
  String get registerUrl => "${this.urlbase}/register";
  String get logUrl => "${this.urlbase}/log";

  Completer<void> completer = Completer<void>();
  bool get initialized => this.completer.isCompleted;
  Future<void> get initializing => this.completer.future;


  Connection();

  Connection.createAndUpdate(String username, String url, int port) {
    this.update(username, url, port);
  }


  Future<void> initialize() async {
    if (this.initialized)
      return;

    // create client
    this.client = Client();

    this.status = ConnectionStatus.disconnected;

    // initialization complete
    this.completer.complete(null);
  }

  void checkInit() {
    if (!this.initialized)
      throw StateError("Connection not initialized");
  }

  void update(String username, String url, int port) {
    // nothing changed
    if (this.username == username && this.url == url && this.port == port)
      return;

    // dispose
    print("[Connection.update] Disposing this");
    this.dispose();

    this.username = username;
    this.url = url;
    this.port = port;

    // re-initialize
    print("[Connection.update] Initializing this");
    this.initialize();

    this.notifyListeners();
  }

  /// connects to the server and sets `this.status` accordingly
  Future<void> connect() async {
    this.checkInit();

    this.status = ConnectionStatus.connecting;
    this.notifyListeners();

    // check url
    print("[Connection.connect] checking url...");
    final response = await this.client.get(this.urlbase).timeout(Duration(seconds: Connection.connectionTimeout), onTimeout: () => null);
    if (response == null || response.statusCode != 200) {
      this.status = ConnectionStatus.serverUnreachable;
      return;
    }

    // try to register
    print("[Connection.connect] trying to register...");
    if (await this.register()) {
      print("[Connection.connect] success!");
      this.status = ConnectionStatus.connected;
    }
    else {
      print("[Connection.connect] failure!");
      this.status = ConnectionStatus.badUsername;
    }
    this.notifyListeners();
  }

  /// disconnects from the server
  void disconnect() {
    this.token = null;
    this.status = ConnectionStatus.disconnected;

    this.notifyListeners();
  }

  Future<dynamic> postJson(String url, dynamic jsonData) async {
    this.checkInit();

    // do request with encoded json
    final response = await this.client.post(
      url,
      body: json.encode(jsonData),
      headers: <String, String>{
        HttpHeaders.contentTypeHeader: "application/json",
      },
    ).timeout(Duration(seconds: Connection.connectionTimeout), onTimeout: () => null);

    // return null if request failed else decode response json
    return (response == null || response.statusCode >= 400) ? null : json.decode(response.body);
  }

  /// tries to register for a UUID token via the API, returns true on success
  Future<bool> register() async {
    final responseJson = await this.postJson(
      this.registerUrl,
      {"name": this.username},
    );

    if (responseJson == null) {
      this.token = null;
      print("[Connection.register] registering ${this.username} failed");
      return false;
    }
    else {
      this.token = responseJson["token"];
      print("[Connection.register] registered ${this.username} for token ${this.token}");
      return true;
    }
  }

  /// sends the String `scannedClass` to the connected server according to the API spec,
  /// does nothing if there is no connection
  Future<void> log(String scannedClass) async {
    // not connected
    if (this.status != ConnectionStatus.connected)
      return;

    final responseJson = await this.postJson(
      this.logUrl,
      {"token": this.token, "class": scannedClass},
    );

    if (responseJson == null)
      print("[Connection.log] logging a scan of ${scannedClass} failed");
  }

  @override
  void dispose() {
    // replace completer by an incomplete one, as from now on the object is disposed
    this.completer = Completer<void>();

    this.client?.close();
    this.client = null;

    this.token = null;
    this.status = ConnectionStatus.disconnected;
  }
}
