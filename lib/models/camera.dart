import "dart:async";
import "dart:io" show Platform;

import "package:camera/camera.dart" as camlib;
import "package:flutter/material.dart";
import "package:image/image.dart" as imglib;

import "package:lemonfinderapp/helpers/convert.dart" show Converter;


typedef OnFrameCallback = void Function(imglib.Image);


class Camera extends ChangeNotifier {
  camlib.CameraController controller;
  camlib.CameraDescription desc;
  camlib.ResolutionPreset res;

  Converter converter;

  OnFrameCallback callback;
  double aspectRatio = double.nan;
  bool streaming = false;

  Completer<void> completer = Completer<void>();
  bool get initialized => this.completer.isCompleted;
  Future<void> get initializing => this.completer.future;

  Camera();

  Camera.createAndUpdate(camlib.CameraDescription desc, camlib.ResolutionPreset res) {
    this.update(desc, res);
  }

  Future<void> initialize() async {
    if (this.initialized)
      return;

    // create new controller and converter
    this.controller = camlib.CameraController(this.desc, this.res, enableAudio: false);
    print("[Camera.initialize] Initializing controller");
    await this.controller.initialize();
    camlib.ImageFormatGroup formatGroup = this.controller.imageFormatGroup;
    formatGroup ??= Platform.isAndroid ? camlib.ImageFormatGroup.yuv420 : camlib.ImageFormatGroup.bgra8888;
    this.converter = Converter(formatGroup, this.controller.resolutionPreset);

    // if we were streaming before, continue streaming
    if (this.streaming)
      this.controller.startImageStream(this.callOnFrameCallback);

    // initialization complete
    this.completer.complete(null);
  }

  void checkInit() {
    if (!this.initialized) {
      print("[Camera.checkInit] Raising");
      throw StateError("Camera not initialized");
    }
  }

  void update(camlib.CameraDescription desc, camlib.ResolutionPreset res) {
    // nothing changed
    if (this.desc == desc && this.res == res)
      return;

    // dispose current controller and initialize new one after that
    print("[Camera.update] Disposing this");
    this.dispose().then((_) async {
      // update config
      this.desc = desc;
      this.res = res;

      // re-initialize
      print("[Camera.update] Initializing this");
      await this.initialize();

      this.notifyListeners();
    });
  }

  void callOnFrameCallback(camlib.CameraImage newFrame) {
    imglib.Image frame = this.converter.camlibToImglib(newFrame);

    // fix orientation
    frame = imglib.copyRotate(frame, this.controller.description.sensorOrientation);

    // find dimensions to crop the frame to, so it has the right aspect ratio
    double newWidth, newHeight;
    if (frame.height * this.aspectRatio < frame.width) {  // use full height
      newWidth = frame.height * this.aspectRatio;
      newHeight = frame.height.toDouble();
    }
    else {  // use full width
      newHeight = frame.width / this.aspectRatio;
      newWidth = frame.width.toDouble();
    }

    // crop image to correct aspect ratio
    final left = frame.width / 2 - newWidth / 2;
    final top = frame.height / 2 - newHeight / 2;
    frame = imglib.copyCrop(frame,
                            left.round(),
                            top.round(),
                            newWidth.round(),
                            newHeight.round());

    callback(frame);
  }

  /// starts a frame stream
  ///
  /// calls `callback` with the current frame from the camera, ratiocropped to
  /// `aspectRatio`
  Future<void> callOnFrame(OnFrameCallback callback, double aspectRatio) async {
    print("[Camera.callOnFrame] Called");
    this.checkInit();

    this.callback = callback;
    this.aspectRatio = aspectRatio;

    // this is the first time calling the method, so we got to start the stream
    if (!streaming) {
      print("[Camera.callOnFrame] Starting stream");
      this.streaming = true;
      await this.controller.startImageStream(this.callOnFrameCallback);
      print("[Camera.callOnFrame] Started stream");
    }
  }

  /// stops the frame stream
  Future<void> stopCallOnFrame() async {
    // this is the first time calling the method, so we got to start the stream
    if (streaming) {
      print("[Camera.callOnFrame] Stopping stream");
      this.streaming = false;
      await this.controller.stopImageStream();
      print("[Camera.callOnFrame] Stopped stream");
    }

    this.callback = null;
    this.aspectRatio = double.nan;
  }

  Future<void> dispose() async {
    if (!this.initialized)
      return;

    // stop frame stream
    await this.stopCallOnFrame();

    // replace completer by an incomplete one, as from now on the object is disposed
    this.completer = Completer<void>();

    // dispose controller
    print("[Camera.dispose] Disposing controller");
    //if (this.controller != null)
    await this.controller?.dispose();
    this.controller = null;
    print("[Camera.dispose] Disposed controller");

    // dispose converter
    this.converter?.dispose();
    this.converter = null;
  }
}
