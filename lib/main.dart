import "package:flutter/material.dart";
import "package:provider/provider.dart";

import "package:lemonfinderapp/models/camera.dart";
import "package:lemonfinderapp/models/connection.dart";
import "package:lemonfinderapp/models/neuralnet.dart";
import "package:lemonfinderapp/models/settings.dart";
import "package:lemonfinderapp/screens/camera.dart";
import "package:lemonfinderapp/screens/settings.dart";


void main() {
  // route observer to make camera screen route aware (https://stackoverflow.com/a/58504433/7841202)
  final routeObserver = RouteObserver<Route>();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<Settings>(
          create: (context) => Settings(),
        ),
        ChangeNotifierProxyProvider<Settings, Camera>(
          create: (context) => Camera(),
          update: (context, settings, camera) {
            print("[main] updated camera model");
            settings.initializing
                .then((_) => camera.update(settings.camera, settings.resolution));
            return camera;
          },
        ),
        ChangeNotifierProxyProvider<Settings, NeuralNet>(
          create: (context) => NeuralNet(),
          update: (context, settings, neuralNet) {
            print("[main] updated neuralNet model");
            settings.initializing
                .then((_) => neuralNet.update(settings.model, settings.activeDelegate));
            return neuralNet;
          },
        ),
        ChangeNotifierProxyProvider<Settings, Connection>(
          create: (context) => Connection(),
          update: (context, settings, connection) {
            print("[main] updated connection model");
            settings.initializing
                .then((_) => connection.update(settings.username, settings.server, settings.port));
            return connection;
          },
        ),
      ],

      child: MaterialApp(
        title: "Lemon Finder",
        theme: ThemeData.dark(),
        routes: {
          CameraScreen.route: (context) => Consumer4<Settings, Camera, NeuralNet, Connection>(
            builder: (context, settings, camera, neuralNet, connection, child) => CameraScreen(settings, camera, neuralNet, connection, routeObserver),
          ),
          SettingsScreen.route: (context) => Consumer4<Settings, Camera, NeuralNet, Connection>(
            builder: (context, settings, camera, neuralNet, connection, child) => SettingsScreen(settings, camera, neuralNet, connection),
          ),
        },
        initialRoute: CameraScreen.route,
        navigatorObservers: [routeObserver],
      ),
    )
  );
}
