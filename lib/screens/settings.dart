import "dart:io";

import "package:camera/camera.dart" as camlib;
import "package:flutter/material.dart";
import "package:settings_ui/settings_ui.dart";

import "package:lemonfinderapp/helpers/convert.dart" as imgconv;
import "package:lemonfinderapp/helpers/utils.dart" as utils;
import "package:lemonfinderapp/models/camera.dart";
import "package:lemonfinderapp/models/connection.dart";
import "package:lemonfinderapp/models/neuralnet.dart";
import "package:lemonfinderapp/models/settings.dart";


class SettingsScreen extends StatefulWidget {
  static final route = "settings";

  final Settings settings;
  final Camera camera;
  final NeuralNet neuralNet;
  final Connection connection;

  SettingsScreen(this.settings, this.camera, this.neuralNet, this.connection, {Key key}) : super(key: key);

  @override
  SettingsScreenState createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  Future<void> initializing;

  TextEditingController usernameController;
  TextEditingController serverController;
  TextEditingController portController;

  @override
  void initState() {
    super.initState();
    this.initializing = this.initialize();
  }

  Future<void> initialize() async {
    await widget.settings.initialize();
    await widget.neuralNet.initialize();
    await widget.connection.initialize();

    this.usernameController = TextEditingController(text: widget.settings.username);
    this.serverController = TextEditingController(text: widget.settings.server);
    this.portController = TextEditingController(text: widget.settings.port.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: FutureBuilder(
        future: this.initializing,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
          case ConnectionState.done:
            return SettingsList(
              sections: <SettingsSection>[
                this.buildCameraSection(context),
                this.buildNeuralNetSection(context),
                this.buildConnectionSection(context),
              ],
            );
          default:
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      ),
    );
  }



  SettingsSection buildCameraSection(BuildContext context) {
    return SettingsSection(
      title: "Camera input",
      tiles: <SettingsTile>[
        this.buildShowModelInputSwitch(context),
        this.buildSelectCameraTile(context),
        this.buildSelectResolutionTile(context),
      ],
    );
  }


  SettingsTile buildShowModelInputSwitch(BuildContext context) {
    return SettingsTile.switchTile(
      title: "Show model input",
      subtitle: "resize the preview to the model input size",
      leading: Icon(Icons.grid_on),
      switchValue: widget.settings.showModelInput,
      onToggle: (value) {
        widget.settings.showModelInput = value;
      },
    );
  }


  SettingsTile buildSelectCameraTile(BuildContext context) {
    return SettingsTile(
      title: "Camera",
      subtitle: Settings.cameraNameLookup[widget.settings.camera.lensDirection],
      leading: Icon(Icons.camera_alt, size: 28),
      onTap: () async {
        widget.settings.camera = await this.showSelectCameraDialog(context);
      },
    );
  }

  Future<camlib.CameraDescription> showSelectCameraDialog(BuildContext context) {
    return showDialog<camlib.CameraDescription>(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text("Select camera"),
        children: widget.settings.availableCameras
          .map<Widget>((desc) => this.buildSelectCameraOption(context, desc))
          .toList(),
      ),
    );
  }

  Widget buildSelectCameraOption(BuildContext context, camlib.CameraDescription desc) {
    return SimpleDialogOption(
      onPressed: () => Navigator.of(context).pop(desc),
      child: ListTile(
        title: Text(Settings.cameraNameLookup[desc.lensDirection]),
        subtitle: Text(desc.name),
        dense: true,
      ),
    );
  }


  SettingsTile buildSelectResolutionTile(BuildContext context) {
    return SettingsTile(
      title: "Resolution",
      subtitle: Settings.resolutionNameLookup[widget.settings.resolution],
      leading: Icon(Icons.high_quality, size: 28),
      onTap: () async {
        widget.settings.resolution = await this.showSelectResolutionDialog(context);
      },
    );
  }

  Future<camlib.ResolutionPreset> showSelectResolutionDialog(BuildContext context) {
    return showDialog<camlib.ResolutionPreset>(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text("Select camera"),
        children: widget.settings.availableResolutions
          .map<Widget>((res) => this.buildSelectResolutionOption(context, res))
          .toList(),
      ),
    );
  }

  Widget buildSelectResolutionOption(BuildContext context, camlib.ResolutionPreset res) {
    final enabled = (res == camlib.ResolutionPreset.low);
    return SimpleDialogOption(
      onPressed: () => Navigator.of(context).pop(enabled ? res : null),
      child: ListTile(
        title: Text(Settings.resolutionNameLookup[res]),
        dense: true,
        enabled: enabled,
      ),
    );
  }



  SettingsSection buildNeuralNetSection(BuildContext context) {
    return SettingsSection(
      title: "Neural net",
      tiles: <SettingsTile>[
        this.buildSelectNeuralNetTile(context),
        this.buildSelectDelegateTile(context),
        this.buildSelectIntervalTile(context),
      ],
    );
  }


  SettingsTile buildSelectNeuralNetTile(BuildContext context) {
    return SettingsTile(
      title: "Neural net model",
      subtitle: widget.settings.model,
      leading: Icon(Icons.description, size: 28),
      onTap: () async {
        widget.settings.model = await this.showSelectNeuralNetDialog(context);
      },
    );
  }

  Future<String> showSelectNeuralNetDialog(BuildContext context) {
    return showDialog<String>(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text("Select neural net"),
        children: widget.settings.availableModels
          .map<Widget>((model) => this.buildSelectNeuralNetOption(context, model))
          .toList(),
      ),
    );
  }

  Widget buildSelectNeuralNetOption(BuildContext context, String model) {
    return SimpleDialogOption(
      onPressed: () => Navigator.of(context).pop(model),
      child: ListTile(
        title: Text(model),
        dense: true,
      ),
    );
  }


  SettingsTile buildSelectDelegateTile(BuildContext context) {
    return SettingsTile(
      title: "Delegate",
      subtitle: Settings.delegateNameLookup[widget.settings.activeDelegate],
      leading: Icon(Icons.memory, size: 28),
      onTap: () async {
        widget.settings.activeDelegate = await this.showSelectDelegateDialog(context);
      },
    );
  }

  Future<DelegateType> showSelectDelegateDialog(BuildContext context) {
    return showDialog<DelegateType>(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text("Select neural net delegate"),
        children: widget.settings.availableDelegates
          .map<Widget>((delegate) => this.buildSelectDelegateOption(context, delegate))
          .toList(),
      ),
    );
  }

  Widget buildSelectDelegateOption(BuildContext context, DelegateType delegate) {
    final enabled = Settings.delegateAvailableLookup[delegate];
    return SimpleDialogOption(
      onPressed: () => Navigator.of(context).pop(enabled ? delegate : null),
      child: ListTile(
        title: Text(Settings.delegateNameLookup[delegate]),
        subtitle: Text(Settings.delegateDescriptionLookup[delegate]),
        enabled: enabled,
        dense: true,
      ),
    );
  }


  SettingsTile buildSelectIntervalTile(BuildContext context) {
    return SettingsTile(
      title: "Scan interval",
      subtitle: Settings.intervalNameLookup[widget.settings.scanInterval],
      leading: Icon(Icons.timer, size: 28),
      onTap: () async {
        widget.settings.scanInterval = await this.showSelectIntervalDialog(context);
      },
    );
  }

  Future<int> showSelectIntervalDialog(BuildContext context) {
    return showDialog<int>(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text("Select neural net delegate"),
        children: widget.settings.availableIntervals
          .map<Widget>((interval) => this.buildSelectIntervalOption(context, interval))
          .toList(),
      ),
    );
  }

  Widget buildSelectIntervalOption(BuildContext context, int interval) {
    return SimpleDialogOption(
      onPressed: () => Navigator.of(context).pop(interval),
      child: ListTile(
        title: Text(Settings.intervalNameLookup[interval]),
        subtitle: Text(Settings.intervalDescriptionLookup[interval]),
        dense: true,
      ),
    );
  }



  SettingsSection buildConnectionSection(BuildContext context) {
    return SettingsSection(
      title: "Connection",
      tiles: <SettingsTile>[
        this.buildEnterUsernameTile(context),
        this.buildEnterServerTile(context),
        this.buildEnterPortTile(context),
        this.buildScanQrCodeTile(context),
        this.buildConnectTile(context),
      ],
    );
  }


  SettingsTile buildEnterUsernameTile(BuildContext context) {
    return SettingsTile(
      title: "Username",
      subtitle: widget.settings.username,
      leading: Icon(Icons.account_circle, size: 28),
      onTap: () async {
        widget.settings.username = await this.showEnterUsernameDialog(context);
      },
    );
  }

  Future<String> showEnterUsernameDialog(BuildContext context) {
    this.usernameController.text = widget.settings.username;
    return showDialog<String>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Username"),
        content: TextField(
          keyboardType: TextInputType.name,
          autofocus: true,
          autocorrect: false,
          controller: this.usernameController,
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("CANCEL"),
            onPressed: () => Navigator.of(context).pop(null),
          ),
          FlatButton(
            child: Text("DONE"),
            onPressed: () => Navigator.of(context).pop(this.usernameController.text.trim()),
          ),
        ],
      ),
    );
  }


  SettingsTile buildEnterServerTile(BuildContext context) {
    return SettingsTile(
      title: "Server URL",
      subtitle: widget.settings.server,
      leading: Icon(Icons.storage, size: 28),
      onTap: () async {
        widget.settings.server = await this.showEnterServerDialog(context);
      },
    );
  }

  Future<String> showEnterServerDialog(BuildContext context) {
    this.serverController.text = widget.settings.server;
    return showDialog<String>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Server URL"),
        content: TextField(
          keyboardType: TextInputType.visiblePassword,  // keyboard with numbers and digits, alternative: url
          autofocus: true,
          autocorrect: false,
          controller: this.serverController,
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("CANCEL"),
            onPressed: () => Navigator.of(context).pop(null),
          ),
          FlatButton(
            child: Text("DONE"),
            onPressed: () => Navigator.of(context).pop(this.serverController.text.trim()),
          ),
        ],
      ),
    );
  }


  SettingsTile buildEnterPortTile(BuildContext context) {
    return SettingsTile(
      title: "Server port",
      subtitle: widget.settings.port.toString(),
      leading: Icon(Icons.import_export, size: 28),
      onTap: () async {
        widget.settings.port = await this.showEnterPortDialog(context);
      },
    );
  }

  Future<int> showEnterPortDialog(BuildContext context) {
    this.portController.text = widget.settings.port.toString();
    return showDialog<int>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Server port"),
        content: TextField(
          keyboardType: TextInputType.number,
          autofocus: true,
          autocorrect: false,
          controller: this.portController,
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("CANCEL"),
            onPressed: () => Navigator.of(context).pop(null),
          ),
          FlatButton(
            child: Text("DONE"),
            onPressed: () => Navigator.of(context).pop(int.tryParse(this.portController.text.trim())),
          ),
        ],
      ),
    );
  }


  SettingsTile buildScanQrCodeTile(BuildContext context) {
    return SettingsTile(
      title: "Scan QR code",
      subtitle: "scan the QR code on the page",
      leading: Icon(Icons.qr_code, size: 28),
      onTap: () async {
        // dispose camera as the barcode scanner needs it
        await widget.camera.dispose();

        // scan qr code
        final scanned = await utils.scanQrCode();
        print("[Settings.buildScanQrCodeTile] scanned ${scanned}");

        // split it
        final split = scanned.split(":");
        if (split.length != 2)
          return;

        widget.settings.server = split[0];
        widget.settings.port = int.tryParse(split[1]);  // returns null if it cant parse it
        widget.connection.connect();
      },
    );
  }


  SettingsTile buildConnectTile(BuildContext context) {
    return SettingsTile(
      title: "Connection status: " + widget.connection.statusMessage,
      subtitle: (widget.connection.status == ConnectionStatus.connected) ? "tap to disconnect" : "tap to connect",
      leading: Icon(Icons.link, size: 28),
      trailing: (widget.connection.status == ConnectionStatus.connecting) ? CircularProgressIndicator() : null,
      onTap: () async {
        switch (widget.connection.status) {
          case ConnectionStatus.connected:
          case ConnectionStatus.connecting:
            widget.connection.disconnect();
            break;
          case ConnectionStatus.disconnected:
          case ConnectionStatus.serverUnreachable:
          case ConnectionStatus.badUsername:
          widget.connection.connect();
            break;

        }
      },
    );
  }



  @override
  void dispose() {
    this.usernameController?.dispose();
    this.serverController?.dispose();
    this.portController?.dispose();
    super.dispose();
  }
}
