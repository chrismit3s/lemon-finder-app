import "dart:async";

import "package:camera/camera.dart" as camlib;
import "package:flutter/material.dart";
import "package:image/image.dart" as imglib;

import "package:lemonfinderapp/helpers/convert.dart" as imgconv;
import "package:lemonfinderapp/helpers/utils.dart" as utils;
import "package:lemonfinderapp/models/camera.dart";
import "package:lemonfinderapp/models/connection.dart";
import "package:lemonfinderapp/models/neuralnet.dart";
import "package:lemonfinderapp/models/settings.dart";
import "package:lemonfinderapp/widgets/framepreview.dart";
import "package:lemonfinderapp/widgets/predictions.dart";


class CameraScreen extends StatefulWidget {
  static final route = "camera";

  final Settings settings;
  final Camera camera;
  final NeuralNet neuralNet;
  final Connection connection;
  final RouteObserver<Route> routeObserver;

  CameraScreen(this.settings, this.camera, this.neuralNet, this.connection, this.routeObserver, {Key key}) : super(key: key) {
    print("[CameraScreen] rebuilt");
  }

  @override
  CameraScreenState createState() => CameraScreenState();
}


class CameraScreenState extends State<CameraScreen> with WidgetsBindingObserver, RouteAware {
  imglib.Image frame;
  List<double> preds;

  Timer _timer;
  Timer get timer => this._timer;
  void set timer(Timer newTimer) {
    // keep only one active timer
    if (this._timer != null)
      this._timer.cancel();
    this._timer = newTimer;
  }

  Future<void> initializing;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);  // makes this widget lifecycle aware

    this.frame = imglib.Image(1, 1);
    this.preds = List<double>.filled(widget.neuralNet.labels.length, 0.0);
  }

  /// waits for every model this screen depends on to be initialized, then starts
  /// the frame stream
  Future<void> initialize() async {
    print("[CameraScreen.initialize] initializing settings");
    await widget.settings.initializing;
    print("[CameraScreen.initialize] initializing camera");
    await widget.camera.initializing;
    print("[CameraScreen.initialize] initializing neuralNet");
    await widget.neuralNet.initializing;
    print("[CameraScreen.initialize] initialized settings, camera, neuralNet");

    // clear any preexisting timer
    this.timer = null;

    // start the frame stream
    await widget.camera.callOnFrame(this.setFrame, widget.neuralNet.imageSize[0] / widget.neuralNet.imageSize[1]);

    if (widget.settings.scanOnTap) {
      // clear the preds
      this.clearPreds();
    }
    else {
      // create scan timer if scan on tap is off
      this.timer = Timer.periodic(
        Duration(seconds: widget.settings.scanInterval),
        (timer) {
          print("[CameraScreen] timer triggered");
          if (widget.neuralNet.initialized)
            this.setPreds();
        },
      );
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // this needs `this.context`, so we can't to this in `this.initState`, as the
    // context isn't available then
    widget.routeObserver.subscribe(this, ModalRoute.of(this.context));  // makes this widget route aware
  }

  /// activates this widget, starts initialization process by calling
  /// `this.initialize`, assigns the corresponding future to `this.initializing`
  void activate() {
    this.initializing = this.initialize();
  }

  /// deactivate this widget, stops the frame stream from the camera for performance
  /// gains when this screen isn't visible and cancel every timer, after calling
  /// this method the widget needs to be reactivated with `this.activate`
  Future<void> deactivate() async {
    // clear any preexisting timer
    this.timer = null;

    // stop the frame stream
    await widget.camera.stopCallOnFrame();
  }

  /// updates the state of the widget to show `frame`
  void setFrame(imglib.Image frame) {
    if (widget.settings.showModelInput)
      frame = imglib.copyResize(frame, width: widget.neuralNet.imageSize[0], height: widget.neuralNet.imageSize[1]);

    this.setState(() {
      this.frame = frame;
    });
  }

  /// runs the neural net on the current frame `this.frame` and updates the state
  /// of the widget to show them
  Future<void> setPreds() async {
    final preds = widget.neuralNet.predict(this.frame);
    this.setState(() {
      this.preds = preds;
    });

    // log the results to server
    final index = utils.argmax(preds);
    final class_ = widget.neuralNet.all_classes[index];
    widget.connection.log(class_);
  }

  /// clears the predictions
  void clearPreds() {
    this.setState(() {
      this.preds = <double>[0.0, 0.0, 0.0, 0.0];
    });
  }

  // this screen is popped => deactivate this widget; only works as this widget is route aware
  @override
  //void didPop() => this.deactivate();
  void didPop() {
    print("[CameraScreen.didPop] cameraScreen popped");
    this.deactivate();
  }

  // this screen becomes visible => activate this widget; only works as this widget is route aware
  @override
  //void didPopNext() => this.activate();
  void didPopNext() {
    print("[CameraScreen.didPopNext] screen on top of cameraScreen popped");
    this.activate();
  }

  // this screen is pushed on top => activate this widget; only works as this widget is route aware
  @override
  //void didPush() => this.activate();
  void didPush() {
    print("[CameraScreen.didPush] cameraScreen pushed");
    this.activate();
  }

  // another screen is pushed over this screen => deactivate this widget; only works as this widget is route aware
  @override
  //void didPushNext() => this.deactivate();
  void didPushNext() {
    print("[CameraScreen.didPushNext] screen pushed on top of cameraScreen");
    this.deactivate();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState newState) {
    if (newState == AppLifecycleState.resumed) {
      print("[CameraScreen.didChangeAppLifecycleState] app in foreground");
      this.activate();
    }
    else {
      print("[CameraScreen.didChangeAppLifecycleState] app in background");
      this.deactivate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lemon Finder"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => Navigator.of(context).pushNamed("settings"),
          ),
        ],
      ),
      body: this.buildBody(context),
      backgroundColor: Colors.black,
    );
  }

  Widget buildBody(BuildContext context) {
    return FutureBuilder(
      future: this.initializing,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
        case ConnectionState.done:
          return this.buildInitialized(context);
        default:
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget buildInitialized(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        this.buildCameraPreview(context),
        Expanded(
          child: this.buildPredictions(context),
        ),
      ],
    );
  }

  Widget buildCameraPreview(BuildContext context) {
    // wrap frame preview in inkwell to handle tap if scanOnTap is set
    if (widget.settings.scanOnTap) {
      return InkWell(
        child: FramePreview(this.frame),
        onTap: () {
          print("[CameraScreen] tap triggered");
          if (widget.neuralNet.initialized)
            this.setPreds();

          // create timer to clear the predictions later
          if (this.timer != null)
            this.timer.cancel();
          this.timer = Timer(
            Duration(seconds: Settings.onTapClearDelay),
            () => this.clearPreds(),
          );
        },
      );
    }
    else {
      return FramePreview(this.frame);
    }
  }

  Widget buildPredictions(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Predictions(
        widget.neuralNet.labels,
        this.preds,
      ),
    );
  }

  @override
  void dispose() {
    this.deactivate();

    widget.routeObserver.unsubscribe(this);  // required for making this widget route aware
    WidgetsBinding.instance.removeObserver(this);  // required for making this widget lifecycle aware
    super.dispose();
  }
}
