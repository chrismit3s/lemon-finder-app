import "dart:typed_data";

import "package:flutter/material.dart";
import "package:image/image.dart" as imglib;


class FramePreview extends StatelessWidget {
  static final encoder = imglib.PngEncoder(level: 1, filter: 0);
  final imglib.Image frame;

  const FramePreview(this.frame, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.memory(
      Uint8List.fromList(FramePreview.encoder.encodeImage(this.frame)),
      gaplessPlayback: true,
      alignment: Alignment.center,
      fit: BoxFit.fill,
      filterQuality: FilterQuality.none,
    );
  }
}
