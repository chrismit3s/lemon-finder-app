import "dart:ui";

import "package:flutter/material.dart";

import "package:lemonfinderapp/helpers/utils.dart" as utils;


class Predictions extends StatelessWidget {
  final List<String> labels;
  final List<double> preds;
  int index;

  Predictions(this.labels, this.preds, {Key key}) : super(key: key) {
    this.index = utils.argmax(this.preds);
    if (this.index < 0 || this.index >= this.preds.length)
      this.index = null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: this.buildHeader(context),
        ),
        ...this.buildLines(context),
      ],
    );
  }

  Widget buildHeader(BuildContext context) {
    return PredictionHeader((this.index == null) ? "" : this.labels[this.index]);
  }

  List<Widget> buildLines(BuildContext context) {
    return utils.zip3(
      this.labels,
      this.preds,
      utils.range(this.labels.length))
        .map((x) => PredictionLine(x.first, x.second.isFinite ? x.second : 0.0, x.third == this.index))
        .toList();
  }
}


class PredictionHeader extends StatelessWidget {
  final String label;

  const PredictionHeader(this.label, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        this.label,
        style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 2.0),
      ),
    );
  }
}


class PredictionLine extends StatelessWidget {
  final double pred;
  final String label;
  final bool isMax;

  const PredictionLine(this.label, this.pred, this.isMax, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Text(
              this.label,
              textAlign: TextAlign.start,
              style: DefaultTextStyle.of(context).style.apply(
                fontWeightDelta: this.isMax ? 3 : 0,
                fontSizeFactor: 1.1,
              ),
            ),
          ),
          Text(
            (this.pred * 100).round().toString() + "%",
            textAlign: TextAlign.end,
            style: DefaultTextStyle.of(context).style.apply(
              fontWeightDelta: this.isMax ? 3 : 0,
              fontSizeFactor: 1.1,
              fontFeatures: [FontFeature.tabularFigures()]
            ),
          ),
        ],
      ),
    );
  }
}
